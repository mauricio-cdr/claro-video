import claroVideoApi from './http.service';

export const fetchFilms = (req) => claroVideoApi.get(
  'list', { params: req.params },
);

export const fetchFilmById = (req) => claroVideoApi.get(
  'data', { params: req.params },
);
