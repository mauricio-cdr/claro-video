import mockAxios from 'axios';
import {
  fetchFilms,
  fetchFilmById,
} from './films.service';

describe('accountService', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should call post property accounts', () => {
    const url = 'list';
    const req = { id: 1, params: {} };
    fetchFilms(req);
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
    expect(mockAxios.get).toHaveBeenCalledWith(
      url,
      { params: {} },
    );
  });

  it('should call post property accounts', () => {
    const url = 'data';
    const req = { id: 1, params: {} };
    fetchFilmById(req);
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
    expect(mockAxios.get).toHaveBeenCalledWith(
      url,
      { params: {} },
    );
  });
});
