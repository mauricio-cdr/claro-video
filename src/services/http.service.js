import axios from 'axios';

const http = () => {
  const baseURL = process.env.REACT_APP_CLARO_VIDEO_API;
  return axios.create({
    baseURL,
    headers: {
      'Content-Type': 'application/json',
    },
  });
};

export default http();
