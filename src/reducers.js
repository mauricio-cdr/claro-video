import {
  combineReducers,
} from 'redux-immutable';
import filmsState from './containers/Films/films.reducer';
import filmDetailState from './containers/FilmDetail/filmsDetail.reducer';

export default combineReducers({
  filmsState,
  filmDetailState,
});
