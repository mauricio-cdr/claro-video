import React from 'react';
import './loading.scss';

const Loading = () => (
  <div className="Loading">
    <div>
      <div className="lds-spinner">
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  </div>
);

export default Loading;
