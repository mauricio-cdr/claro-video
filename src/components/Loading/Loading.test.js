import React from 'react';
import { shallow } from 'enzyme';
import Loading from './Loading';

describe('<FilmsDetailPage />', () => {
  it('Should enders without crashing', () => {
    const component = shallow(
      <Loading />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should have Card class', () => {
    const component = shallow(
      <Loading />,
    );
    expect(component.find('.Loading')).toHaveLength(1);
  });
});
