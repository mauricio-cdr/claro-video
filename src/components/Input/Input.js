import React from 'react';
import PropTypes from 'prop-types';
import './input.scss';

export const Input = (props) => {
  const {
    onChange, value, className, type,
  } = props;
  return (
    <input
      className={`Input ${className}`}
      onChange={onChange}
      value={value}
      type={type}
    />
  );
};

Input.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.string,
  className: PropTypes.string,
  type: PropTypes.string,
};

Input.defaultProps = {
  onChange: (a) => a,
  value: '',
  className: '',
  type: '',
};

export default Input;
