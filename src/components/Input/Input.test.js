import React from 'react';
import { shallow } from 'enzyme';
import { Input } from './Input';

describe('<Input />', () => {
  it('Should enders without crashing', () => {
    const component = shallow(
      <Input />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should have Card class', () => {
    const component = shallow(
      <Input />,
    );
    expect(component.find('.Input')).toHaveLength(1);
  });
});
