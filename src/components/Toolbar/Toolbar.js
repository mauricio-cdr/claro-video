import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/clarovideo-logo-sitio.svg';
import './Toolbar.scss';

const Toolbar = () => (
  <header className="Toolbar">
    <Link to="/" className="link-home"><img src={logo} alt="logo-claro-video" /></Link>
  </header>
);

export default Toolbar;
