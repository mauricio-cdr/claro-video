import React from 'react';
import { shallow } from 'enzyme';
import Toolbar from './Toolbar';

describe('<Card />', () => {
  it('Should enders without crashing', () => {
    const component = shallow(
      <Toolbar />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should have Toolbar class', () => {
    const component = shallow(
      <Toolbar />,
    );
    expect(component.find('.Toolbar')).toHaveLength(1);
  });
});
