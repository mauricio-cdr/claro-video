import React from 'react';
import PropTypes from 'prop-types';
import Toolbar from '../components/Toolbar';
import './App.scss';

const App = (props) => {
  const { children } = props;
  return (
    <div className="App theme-dark">
      <Toolbar />
      <main className="App-content">
        {children}
      </main>
    </div>
  );
};

App.propTypes = {
  children: PropTypes.element.isRequired,
};

export default App;
