import { fetchFilmById } from '../../services/films.service';
import {
  GET_FILM_BY_ID_LOADING,
  GET_FILM_BY_ID_FAILED,
  GET_FILM_BY_ID_SUCCESS,
} from '../../constants/action.types';

export const getFilm = (req) => ({
  types: [
    GET_FILM_BY_ID_LOADING,
    GET_FILM_BY_ID_SUCCESS,
    GET_FILM_BY_ID_FAILED,
  ],
  promise: fetchFilmById(req),
});
