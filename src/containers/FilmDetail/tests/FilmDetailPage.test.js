import React from 'react';
import { shallow, mount } from 'enzyme';
import { FilmsDetailPage } from '../FilmDetailPage';

describe('<FilmsDetailPage />', () => {
  it('Should renders without crashing', () => {
    const component = shallow(
      <FilmsDetailPage
        film={{}}
        match={{ params: { filmId: 1 } }}
        getFilm={(a) => a}
      />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should render content when no loading', () => {
    const component = mount(
      <FilmsDetailPage
        film={{}}
        isLoading={false}
        match={{ params: { filmId: 1 } }}
        getFilm={(a) => a}
      />,
    );
    expect(component.find('.FilmDetail-container')).toHaveLength(1);
  });

  it('Should render loader when no loading', () => {
    const component = mount(
      <FilmsDetailPage
        film={{}}
        isLoading
        match={{ params: { filmId: 1 } }}
        getFilm={(a) => a}
      />,
    );
    expect(component.find('.Loading')).toHaveLength(1);
  });
});
