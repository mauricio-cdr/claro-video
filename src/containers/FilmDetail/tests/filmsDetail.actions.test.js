import {
  getFilm,
} from '../filmsDetail.actions';
import { fetchFilmById } from '../../../services/films.service';
import {
  GET_FILM_BY_ID_LOADING,
  GET_FILM_BY_ID_FAILED,
  GET_FILM_BY_ID_SUCCESS,
} from '../../../constants/action.types';

describe('Film Detail actions', () => {
  it('Should create a getFilm action', () => {
    const expectedAction = {
      types: [
        GET_FILM_BY_ID_LOADING,
        GET_FILM_BY_ID_SUCCESS,
        GET_FILM_BY_ID_FAILED,
      ],
      promise: fetchFilmById({ params: {} }),
    };
    expect(getFilm({ params: {} })).toEqual(expectedAction);
  });
});
