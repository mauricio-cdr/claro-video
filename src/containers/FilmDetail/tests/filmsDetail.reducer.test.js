import reducer from '../filmsDetail.reducer';
import {
  GET_FILM_BY_ID_FAILED,
  GET_FILM_BY_ID_LOADING,
  GET_FILM_BY_ID_SUCCESS,
} from '../../../constants/action.types';

describe('FilmDetailReducer', () => {
  const state = {
    isLoading: false,
    film: {},
    error: null,
  };
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(state);
  });

  it('Should update loading state value to true', () => {
    const expectedState = {
      isLoading: true,
      film: {},
      error: null,
    };
    const literalAction = {
      type: GET_FILM_BY_ID_LOADING,
    };
    expect(reducer(state, literalAction)).toEqual(expectedState);
  });

  it('Should update error state', () => {
    const expectedState = {
      isLoading: false,
      film: {},
      error: { code: '500', msg: 'error' },
    };
    const literalAction = {
      type: GET_FILM_BY_ID_FAILED,
      error: { code: '500', msg: 'error' },
    };
    expect(reducer(state, literalAction)).toEqual(expectedState);
  });

  it('Should update film state on success', () => {
    const expectedState = {
      isLoading: false,
      film: { id: 1, title: 'movie 1' },
      error: null,
    };
    const literalAction = {
      type: GET_FILM_BY_ID_SUCCESS,
      result: { data: { msg: 'OK', response: { group: { common: { id: 1, title: 'movie 1' } } } } },
    };
    expect(reducer(state, literalAction)).toEqual(expectedState);
  });
});
