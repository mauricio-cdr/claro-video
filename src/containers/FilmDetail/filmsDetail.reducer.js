
import {
  GET_FILM_BY_ID_FAILED,
  GET_FILM_BY_ID_LOADING,
  GET_FILM_BY_ID_SUCCESS,
} from '../../constants/action.types';

const initialState = {
  isLoading: false,
  film: {},
  error: null,
};

const filmDetailReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FILM_BY_ID_LOADING:
      return { ...state, isLoading: true };
    case GET_FILM_BY_ID_SUCCESS: {
      const { msg, response } = action.result.data;
      if (msg !== 'OK') return { ...state, isLoading: false };
      return { ...state, isLoading: false, film: response.group.common };
    }
    case GET_FILM_BY_ID_FAILED:
      return { ...state, isLoading: false, error: action.error };
    default:
      return state;
  }
};

export default filmDetailReducer;
