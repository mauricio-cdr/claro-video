/* eslint-disable react/jsx-one-expression-per-line */
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getFilm as fetchFilm } from './filmsDetail.actions';
import LoadingOverlay from '../../components/Loading';
import './FilmDetailPage.scss';

export const FilmsDetailPage = (props) => {
  const { getFilm, isLoading } = props;
  const { film, match: { params: filmId } } = props;

  useEffect(() => {
    const params = {
      device_id: 'web',
      device_category: 'web',
      device_model: 'web',
      device_type: 'web',
      format: 'json',
      device_manufacturer: 'generic',
      authpn: 'webclient',
      authpt: 'tfg1h3j4k6fd7',
      api_version: 'v5.88',
      region: 'mexico',
      HKS: 'lf2pph465i91vjj21rdv1a5mb4',
      group_id: filmId.filmId,
    };
    getFilm({ params });
  }, [getFilm, filmId.filmId]);
  const { extendedcommon } = film;
  return (
    <section className="FilmDetail">
      {isLoading
        ? <LoadingOverlay />
        : (
          <div className="FilmDetail-container">
            <div className="FilmDetail-preview">
              <img src={film.image_large} alt={film.title} />
            </div>
            {extendedcommon && (
              <div className="FilmDetail-info">
                <h1>{film.title}</h1>
                <h3>{extendedcommon.media.originaltitle}</h3>
                <p>{film.large_description}</p>
                <div className="FilmDetail-indicators">
                  <strong><time>{extendedcommon.media.publishyear}</time></strong>
                  <time>{ film.duration} h</time>
                </div>
                <p><strong>Generos:</strong>{extendedcommon.genres.genre.map((a) => ` ${a.name}, `)}</p>
              </div>
            )}
          </div>
        )}
    </section>
  );
};

FilmsDetailPage.propTypes = {
  getFilm: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  film: PropTypes.shape({
    title: PropTypes.string,
    duration: PropTypes.string,
    extendedcommon: PropTypes.shape({
      media: {
        originaltitle: PropTypes.string,
      },
      genres: {
        genre: PropTypes.arrayOf(PropTypes.shape({})),
      },
    }),
    image_large: PropTypes.string,
    large_description: PropTypes.string,
  }),
  error: PropTypes.shape({}),
  match: PropTypes.shape({
    params: {
      filmId: 1,
    },
  }).isRequired,
};

FilmsDetailPage.defaultProps = {
  film: {},
  error: null,
  isLoading: false,
};

const mapStateToProps = (state) => ({
  film: state.get('filmDetailState').film,
  isLoading: state.get('filmDetailState').isLoading,
  error: state.get('filmDetailState').error,
});

const mapDispatchToProps = (dispatch) => ({
  getFilm: (req) => dispatch(fetchFilm(req)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FilmsDetailPage);
