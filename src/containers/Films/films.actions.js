import { fetchFilms } from '../../services/films.service';
import {
  GET_FILM_LIST_LOADING,
  GET_FILM_LIST_FAILED,
  GET_FILM_LIST_SUCCESS,
} from '../../constants/action.types';

export const getFilms = (req) => ({
  types: [
    GET_FILM_LIST_LOADING,
    GET_FILM_LIST_SUCCESS,
    GET_FILM_LIST_FAILED,
  ],
  promise: fetchFilms(req),
});
