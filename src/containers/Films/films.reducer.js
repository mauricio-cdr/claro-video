import {
  GET_FILM_LIST_LOADING,
  GET_FILM_LIST_SUCCESS,
  GET_FILM_LIST_FAILED,
} from '../../constants/action.types';

const initialState = {
  isLoadingList: true,
  films: [],
  error: null,
};

const filmsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FILM_LIST_LOADING:
      return { ...state, isLoadingList: true };
    case GET_FILM_LIST_SUCCESS: {
      const { msg, response } = action.result.data;
      if (msg !== 'OK') return { ...state, isLoading: true };
      return {
        ...state,
        isLoadingList: false,
        films: response.groups.map((a) => {
          // eslint-disable-next-line no-param-reassign
          a.title_lowercase = a.title.toLowerCase(); return a;
        }),
      };
    }
    case GET_FILM_LIST_FAILED:
      return { ...state, isLoadingList: false, error: action.error };
    default:
      return state;
  }
};

export default filmsReducer;
