import React from 'react';
import { shallow } from 'enzyme';
import FilmCard from './FilmCard';

describe('<FilmsDetailPage />', () => {
  it('Should enders without crashing', () => {
    const component = shallow(
      <FilmCard data={{ id: 1 }} />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should have FilmCard class', () => {
    const component = shallow(
      <FilmCard data={{ id: 1 }} />,
    );
    expect(component.find('.FilmCard')).toHaveLength(1);
  });
});
