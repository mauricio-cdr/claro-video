/* eslint-disable camelcase */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Card from '../../../../components/Card';
import './filmCard.scss';

const FilmCard = (props) => {
  const { data: { id, image_small, title } } = props;
  return (
    <Link to={`/films/${id}`}>
      <Card className="FilmCard">
        <img src={image_small} alt={title} />
        <div className="overlay" />
      </Card>
    </Link>
  );
};

FilmCard.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    image_small: PropTypes.string,
  }).isRequired,
};

export default FilmCard;
