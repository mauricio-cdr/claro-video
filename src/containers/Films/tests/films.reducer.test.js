import reducer from '../films.reducer';
import {
  GET_FILM_LIST_LOADING,
  GET_FILM_LIST_SUCCESS,
} from '../../../constants/action.types';

describe('FilmDetailReducer', () => {
  const state = {
    isLoadingList: true,
    films: [],
    error: null,
  };
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(state);
  });

  it('Should update loading state to true', () => {
    const expectedState = {
      isLoadingList: true,
      films: [],
      error: null,
    };
    const literalAction = {
      type: GET_FILM_LIST_LOADING,
    };
    expect(reducer(state, literalAction)).toEqual(expectedState);
  });

  it('Should update films list', () => {
    const expectedState = {
      isLoadingList: false,
      films: [{ id: 1, title: 'ABC', title_lowercase: 'abc' }],
      error: null,
    };
    const literalAction = {
      type: GET_FILM_LIST_SUCCESS,
      result: { data: { msg: 'OK', response: { groups: [{ id: 1, title: 'ABC' }] } } },
    };
    expect(reducer(state, literalAction)).toEqual(expectedState);
  });
});
