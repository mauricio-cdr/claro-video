import {
  getFilms,
} from '../films.actions';
import { fetchFilms } from '../../../services/films.service';
import {
  GET_FILM_LIST_LOADING,
  GET_FILM_LIST_FAILED,
  GET_FILM_LIST_SUCCESS,
} from '../../../constants/action.types';

describe('Films actions', () => {
  it('should create a getFilms action', () => {
    const expectedAction = {
      types: [
        GET_FILM_LIST_LOADING,
        GET_FILM_LIST_SUCCESS,
        GET_FILM_LIST_FAILED,
      ],
      promise: fetchFilms({ params: {} }),
    };
    expect(getFilms({ params: {} })).toEqual(expectedAction);
  });
});
