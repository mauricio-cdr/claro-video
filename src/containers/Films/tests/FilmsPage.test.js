import React from 'react';
import { shallow } from 'enzyme';
import { FilmsPage } from '../FilmsPage';

describe('<FilmsPage />', () => {
  test('Should enders without crashing', () => {
    const component = shallow(
      <FilmsPage
        getFilms={(a) => a}
      />,
    );
    expect(component).toMatchSnapshot();
  });
});
