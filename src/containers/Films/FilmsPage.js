import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getFilms as fetchFilms } from './films.actions';
import FilmCard from './components/FilmCard';
import Input from '../../components/Input';
import LoadingOverlay from '../../components/Loading';
import './FilmsPage.scss';

export const FilmsPage = (props) => {
  const { getFilms, loading } = props;
  const { films } = props;

  const [searchText, updateSearch] = useState('');

  useEffect(() => {
    const params = {
      api_version: 'v5.88',
      authpn: 'webclient',
      authpt: 'tfg1h3j4k6fd7',
      format: 'json',
      region: 'mexico',
      device_id: 'web',
      device_category: 'web',
      device_model: 'web',
      device_type: 'web',
      device_so: 'chrome',
      device_manufacturer: 'generic',
      HKS: 'lf2pph465i91vjj21rdv1a5mb4',
      quantity: '40',
      order_way: 'DESC',
      order_id: '200',
      level_id: 'GPS',
      from: '0',
      node_id: '43864',
    };
    getFilms({ params });
  }, [getFilms]);

  const filmList = films.filter((film) => (
    film.title_lowercase.search(searchText.toLowerCase()) > -1));
  return (
    <section className="Films">
      { loading
        ? <LoadingOverlay />
        : (
          <div className="Films-container">
            <aside className="search">
              <span>Busca:</span>
              <Input
                value={searchText}
                className="Films input-search"
                onChange={(t) => updateSearch(t.target.value)}
              />
            </aside>
            <div className="infinite-scroll">
              <div className="list">
                {filmList.length === 0
                  ? <span className="Films not-found-label">Sin resultados</span>
                  : filmList.map((film) => <FilmCard key={film.id} data={film} />)}
              </div>
            </div>
          </div>
        )}
    </section>
  );
};

FilmsPage.propTypes = {
  films: PropTypes.arrayOf(PropTypes.shape()),
  getFilms: PropTypes.func.isRequired,
  loading: PropTypes.bool,
};

FilmsPage.defaultProps = {
  films: [],
  loading: false,
};

const mapStateToProps = (state) => ({
  films: state.get('filmsState').films,
  error: state.get('filmsState').error,
  loading: state.get('filmsState').isLoadingList,
});

const mapDispatchToProps = (dispatch) => ({
  getFilms: (req) => dispatch(fetchFilms(req)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FilmsPage);
