import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import App from './containers/App';
import FilmsPage from './containers/Films/FilmsPage';
import FimlDetailPage from './containers/FilmDetail/FilmDetailPage';


const publicRoutes = () => (
  <App>
    <Switch>
      <Route exact path="/" component={FilmsPage} />
      <Route exact path="/films" component={FilmsPage} />
      <Route exact path="/films/:filmId" component={FimlDetailPage} />
    </Switch>
  </App>
);

const router = () => (
  <Router>
    <Switch>
      {publicRoutes()}
      <Route path="**" component={FilmsPage} />
    </Switch>
  </Router>
);
export default router;
