# CLARO VIDEO

![claro-logo](https://upload.wikimedia.org/wikipedia/commons/0/0e/LogoClaroVid.png)

Copia de pantallas de la interfaz de [claro-video](http://www.clarovideo.com) utilizando reactJS - Redux.

![redux flow](https://camo.githubusercontent.com/9de527b9432cc9244dc600875b46b43311918b59/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f6d656469612d702e736c69642e65732f75706c6f6164732f3336343831322f696d616765732f323438343739302f415243482d5265647578322d657874656e6465642d7265616c2d6465636c657261746976652e676966)

[DEMO DEL PROYECTO](https://claro-video-app.herokuapp.com)

## Tabla de contenído

* [Tecnología](#tech)
* [Estructuro del proyecto](#structure)
* [Configuracion](#conf)
* [DevTools](#devTools)
* [Tests](#test)
* [Ejecutar](#run)
* [Construir](#build)

**Libraries:**

* [NodeJS](https://nodejs.org/es/)
* [React](https://facebook.github.io/react/)
* [CreateReactApp](https://github.com/facebook/create-react-app)
* [Sass](https://sass-lang.com/)
* [Jest](https://facebook.github.io/jest/)
* [MaterialUI](https://material-ui.com/)

### Transpilador JS

* [babeljs](https://babeljs.io/)

## <a name="structure"></a> Project Structure

| File name     |   Description      |
| ------------ | -------------------|
| config/          | Archivos de configuración de webpack y jest |
| node_modules/    | Modulos de  Node |
| public/      | Archivoscomo index.html, fav.icon| |
| src/      | Componentes y más de la aplicación |
| src/assets     | Assets generales (iconos e imagenes)|
| src/components      | Componentes cómunes de la aplicación como Button,Card|
| src/containers      | Cómponenntes contenedores (Páginas) "La mayoría Se conectan a redux"|
| src/middlewares     | Middlewares, como logguers, o funciones para manejar las peticiones al API|
| *.eslintrc*     | Configuracion de reglas para el código |
| *.gitignore*     | Patrones y nombres de archivos para excluir archivos con **git**  |
| *package.json*     |  Archivo con la lista de dependencias necesarias y otras configuraciones generales |
| *.gitlab-ci.yml*     |  Archivo con la configuración para el ciclo  de CI|
| *README.md*   | README file|
| *.env*   | Arhivo con las variables de entorno "No debería versionarse"|
## <a name="conf"></a> Configuración de entorno

* Instalar node [NVM](https://github.com/creationix/nvm#installation) o [NVM WINDOWS](https://github.com/coreybutler/nvm-windows)

```shell
  nvm install stable
```

* Instalar [YARN](https://yarnpkg.com/lang/en/docs/install/) project dependencies

## <a name="devTools"></a> DevTools

* Instalar la extención [Redux  DevTools](http://extension.remotedev.io/#installation)


## <a name="run"></a> Ejecutar el proyecto

   * intala dependecias
```shell
    yarn install
```
   * Ejecuta el proyecto
```shell
    yarn start
```

Abre tu navegador en [http://localhost:3000/](http://localhost:3000/)

## <a name="run"></a> Build project

Generar el build del  proyecto

```shell
    yarn build
```

## <a name="test"></a> Testing

Ejecutar las prubeas unitarias.

```shell
    yarn test
```

Ejecutar las pruebas  unitarias y  obtener un reporte de la covertura del código

```shell
    yarn coverage
```

## Versioning

## Autor

* **Mauricio Campos** en [gitlab](https://gitlab.com/mauricio-cdr) y en [github](https://github.com/mauricio-cdr)
